#!/usr/bin/env python3
import cv2 as cv
import numpy as np
from functools import partial

import rclpy
from sensor_msgs.msg import Image
from rclpy.node import Node
from cv_bridge import CvBridge

class HSVHelper(Node):
    def __init__(self):
        super().__init__("hsv_helper")

        self.max_values = np.array([180, 255, 255])
        self.low = np.array([0, 0, 0])
        self.high = self.max_values

        self.window_capture_name = "Video Capture"
        self.window_detection_name = "Object_Detection"

        self.trackbar_low_names = ["low_h", "low_s", "low_v"]
        self.trackbar_high_names = ["high_h", "high_s", "high_v"]

        cv.namedWindow(self.window_capture_name, cv.WINDOW_NORMAL)
        cv.namedWindow(self.window_detection_name, cv.WINDOW_NORMAL)

        for i in range(3):
            cv.createTrackbar(self.trackbar_low_names[i], self.window_detection_name, self.low[i], self.max_values[i], partial(self.low_callback, trackbar=self.trackbar_low_names[i], index=i))
            cv.createTrackbar(self.trackbar_high_names[i], self.window_detection_name, self.high[i], self.max_values[i], partial(self.high_callback, trackbar=self.trackbar_high_names[i], index=i))

        self.cv_bridge = CvBridge()
        self.create_subscription(Image, "/processed_image", self.img_cb, 10)

    def low_callback(self, value, trackbar, index):
        self.low[index] = min(self.high[index]-1, value)
        cv.setTrackbarPos(self.trackbar_low_names[index], self.window_detection_name, self.low[index])

    def high_callback(self, value, trackbar, index):
        self.high[index] = max(value, self.low[index]+1)
        cv.setTrackbarPos(self.trackbar_low_names[index], self.window_detection_name, self.low[index])

    def img_cb(self, msg):
        frame = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding="rgb8")
        frame = cv.cvtColor(frame, cv.COLOR_RGB2BGR)

        blur = cv.GaussianBlur(frame,(3,3), 0)

        # convert to HSV
        hsv = cv.cvtColor(blur, cv.COLOR_BGR2HSV)

        # threshold everything in the wanted range
        mask = cv.inRange(hsv, self.low, self.high)

        # take every pixel in original corresponding the color mask
        res = cv.bitwise_and(frame, frame, mask=mask)

        cv.imshow(self.window_capture_name,frame)
        cv.imshow(self.window_detection_name,mask)
        cv.imshow('res',res)
        cv.waitKey(1)


def main():
    rclpy.init()
    node_handle = HSVHelper()

    try:
        rclpy.spin(node_handle)
    except KeyboardInterrupt:
        pass

    cv.destroyAllWindows()
    rclpy.shutdown()

if __name__ == '__main__':
    main()