#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2 as cv
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image

class ImageProcessingNode(Node):
    def __init__(self):
        super().__init__('image_processing_node')

        self.subscription = self.create_subscription(
            Image,
            'traffic_light_roi',
            self.roi_callback,
            10
        )
        
        self.publisher_processing = self.create_publisher(Image, 'processed_image', 10)
        self.publisher_traffic_light_color = self.create_publisher(String, 'traffic_light_color', 10)
        
        self.cv_bridge = CvBridge()
        self.frame = None 
        self.detection_done = False

    def roi_callback(self, msg):

        if msg.data:
            cv_image = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='rgb8')
            self.frame = cv_image
            preprocessed_frame, color = self.preprocess(self.frame)
            self.get_logger().info(f"Traffic light color: {color}")

            # Publish the detected color
            color_msg = String()
            color_msg.data = color
            self.publisher_traffic_light_color.publish(color_msg)

            processed_msg = self.cv_bridge.cv2_to_imgmsg(preprocessed_frame, encoding='rgb8')
            processed_msg.header = msg.header
            self.publisher_processing.publish(processed_msg)

            self.detection_done = True  # Set the flag

        else:
            if self.detection_done:
                # Publish "nothing" when there is no image data
                color_msg = String()
                color_msg.data = "nothing"
                self.publisher_traffic_light_color.publish(color_msg)

                self.get_logger().info("No image data received. Setting color to: nothing")
                self.detection_done = False  # Reset the flag



    def preprocess(self, image):

        average_normalized = cv.blur(image, (5, 5))
        average = cv.boxFilter(average_normalized, -1, (5, 5))
        gauss = cv.GaussianBlur(average, (5, 5), 0)

        frame = cv.cvtColor(gauss, cv.COLOR_RGB2BGR)

        blur = cv.GaussianBlur(frame,(3,3), 0)

        # Convert the preprocessed image to HSV color space
        hsv_image = cv.cvtColor(blur, cv.COLOR_BGR2HSV)

        # HSV ranges for Red, Yellow, and Green colors
        red_lower_bound = np.array([0, 150, 150])    
        red_upper_bound = np.array([12, 255, 255]) 
        yellow_lower_bound = np.array([20, 150, 150]) 
        yellow_upper_bound = np.array([30, 255, 255])
        green_lower_bound = np.array([50, 150, 150]) 
        green_upper_bound = np.array([70, 255, 255]) 

        red_mask = cv.inRange(hsv_image, red_lower_bound, red_upper_bound)
        yellow_mask = cv.inRange(hsv_image, yellow_lower_bound, yellow_upper_bound)
        green_mask = cv.inRange(hsv_image, green_lower_bound, green_upper_bound)

        kernel = np.ones((3, 3), np.uint8)

        red_mask_opened = cv.morphologyEx(red_mask, cv.MORPH_OPEN, kernel)
        yellow_mask_opened = cv.morphologyEx(yellow_mask, cv.MORPH_OPEN, kernel)
        green_mask_opened = cv.morphologyEx(green_mask, cv.MORPH_OPEN, kernel)

        red_filtered_image = cv.bitwise_and(image, image, mask=red_mask_opened)
        yellow_filtered_image = cv.bitwise_and(image, image, mask=yellow_mask_opened)
        green_filtered_image = cv.bitwise_and(image, image, mask=green_mask_opened)

        preprocessed_frame = red_filtered_image + yellow_filtered_image + green_filtered_image

        red_circles = cv.HoughCircles(red_mask_opened, cv.HOUGH_GRADIENT, dp=1, minDist=red_mask_opened.shape[0]/4, param1=100, param2=5)
        yellow_circles = cv.HoughCircles(yellow_mask_opened, cv.HOUGH_GRADIENT, dp=1, minDist=yellow_mask_opened.shape[0]/4, param1=100, param2=5)
        green_circles = cv.HoughCircles(green_mask_opened, cv.HOUGH_GRADIENT, dp=1, minDist=green_mask_opened.shape[0]/4, param1=100, param2=5)

        color = "nothing"
        if red_circles is not None:
            color = "red"
            red_circles = np.uint16(np.around(red_circles))
            for circle in red_circles[0, :]:
                center = (circle[0], circle[1])
                radius = circle[2]
                cv.circle(preprocessed_frame, center, radius, (0, 0, 255), 2)

        elif yellow_circles is not None:
            color = "yellow"
            yellow_circles = np.uint16(np.around(yellow_circles))
            for circle in yellow_circles[0, :]:
                center = (circle[0], circle[1])
                radius = circle[2]
                cv.circle(preprocessed_frame, center, radius, (0, 255, 255), 2)

        elif green_circles is not None:
            color = "green"
            green_circles = np.uint16(np.around(green_circles))
            for circle in green_circles[0, :]:
                center = (circle[0], circle[1])
                radius = circle[2]
                cv.circle(preprocessed_frame, center, radius, (0, 0, 255), 2)

        else:
            color ="nothing"      

        return preprocessed_frame, color

def main(args=None):
    rclpy.init(args=args)
    image_processing_node = ImageProcessingNode()
    rclpy.spin(image_processing_node)
    image_processing_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
