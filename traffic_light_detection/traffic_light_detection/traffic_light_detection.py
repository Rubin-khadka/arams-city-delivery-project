#!/usr/bin/env python3

import os
import rclpy
from rclpy.node import Node
import cv2
import math

import sys
sys.path.append(os.path.expanduser('/home/rubin/darknet/')) 
import darknet

from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from std_msgs.msg import Header
from cv_bridge import CvBridge
from nav_msgs.msg import Odometry
import tf2_ros
import tf_transformations

class TrafficLightDetectionNode(Node):
    def __init__(self):
        super().__init__('traffic_light_detection')

        self.network, self.class_names, _ = darknet.load_network(
            "/home/rubin/ros2_ws/src/traffic_light_detection/weights/yolov4-tiny.cfg",
            "/home/rubin/ros2_ws/src/traffic_light_detection/weights/arams.data",
            "/home/rubin/ros2_ws/src/traffic_light_detection/weights/yolov4-tiny.weights",
            batch_size=1
        )

        self.cv_bridge = CvBridge()
        self.image_sub = self.create_subscription(
            Image,
            '/prius/front_camera/image_raw',  
            self.image_callback,
            qos_profile=10
        )

        self.traffic_light_roi_pub = self.create_publisher(Image, 'traffic_light_roi', qos_profile=10)
        self.image_transport_pub = self.create_publisher(Image, 'detected_traffic_light', qos_profile=10)
    
        self.car_orientation = None
        self.odom_subscriber = self.create_subscription(
            Odometry,
            '/prius/odom',
            self.odom_callback,
            qos_profile=10
        )

    def odom_callback(self, msg):
        orientation = msg.pose.pose.orientation
        self.car_orientation = tf_transformations.euler_from_quaternion([orientation.x, orientation.y, orientation.z, orientation.w])[2]


    def image_callback(self, msg):
        
        cv_image = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='rgb8')
        detections = self.image_detection(cv_image)
        self.publish_detected_image(cv_image, detections, msg.header)


    def image_detection(self, image):

        img_height, img_width, _ = image.shape

        width = darknet.network_width(self.network)
        height = darknet.network_height(self.network)

        darknet_image = darknet.make_image(width, height, 3)
        thresh = 0.5

        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_resized = cv2.resize(image_rgb, (width, height), interpolation=cv2.INTER_LINEAR)

        resized_height, resized_width, _ = image_resized.shape

        darknet.copy_image_from_bytes(darknet_image, image_resized.tobytes())

        detections = darknet.detect_image(self.network, self.class_names, darknet_image, thresh=thresh)
        darknet.free_image(darknet_image)

        if detections:
            print(f"Number of detections: {len(detections)}")
            for idx, (label, confidence, bbox) in enumerate(detections):
                x, y, w, h = bbox

                width_scale = img_width / width
                height_scale = img_height / height

                x = int((x - w/2) * width_scale)
                y = int((y - h/2) * height_scale)
                w = int(w * width_scale)
                h = int(h * height_scale)

                print(f"Detection {idx + 1}:")
                print(f"  - Confidence: {confidence}")

            return detections
        else:
            #print("No traffic light detections.")
            return []

    def publish_detected_image(self, image, detections, header):

        img_height, img_width, _ = image.shape

        if detections:
            min_angle = float('inf')
            selected_traffic_light = None

            for idx, (label, confidence, bbox) in enumerate(detections):
                x, y, w, h = bbox
                x = int((x - w / 2) * img_width / 416)
                y = int((y - h / 2) * img_height / 416)
                w = int(w * img_width / 416)
                h = int(h * img_height / 416)

                # Calculate the center of the traffic light bounding box
                center_x = x + w / 2
                center_y = y + h / 2

                # Calculate the angle between the car's orientation and the traffic light
                angle_to_traffic_light = math.atan2(center_y - img_height / 2, center_x - img_width / 2) - self.car_orientation

                # Normalize the angle to be between -pi and pi
                if angle_to_traffic_light > math.pi:
                    angle_to_traffic_light -= 2 * math.pi

                elif angle_to_traffic_light < -math.pi:
                    angle_to_traffic_light += 2 * math.pi

                # Find the traffic light with the smallest angle
                if abs(angle_to_traffic_light) < min_angle:
                    min_angle = abs(angle_to_traffic_light)
                    selected_traffic_light = detections[idx]

                # Draw bounding box on the image, draws blue box
                cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

            if selected_traffic_light:
                x, y, w, h = selected_traffic_light[2]
                x = int((x - w / 2) * img_width / 416)
                y = int((y - h / 2) * img_height / 416)
                w = int(w * img_width / 416)
                h = int(h * img_height / 416)
                closest_roi = image[y:y + h, x:x + w]

                if closest_roi.size > 0:
                    closest_roi_msg = self.cv_bridge.cv2_to_imgmsg(closest_roi, encoding='rgb8')
                    closest_roi_msg.header = header
                    self.traffic_light_roi_pub.publish(closest_roi_msg)
                    return
                
        detected_msg = self.cv_bridge.cv2_to_imgmsg(image, encoding='rgb8')
        detected_msg.header = header
        self.image_transport_pub.publish(detected_msg)

def main(args=None):
    rclpy.init(args=args)
    node = TrafficLightDetectionNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()