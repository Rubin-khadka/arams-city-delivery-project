from setuptools import setup
import os
from glob import glob

package_name = 'traffic_light_detection'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/weights', ['weights/yolov4-tiny.cfg', 'weights/yolov4-tiny.weights', 'weights/arams_class.names']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='rubin',
    maintainer_email='rubin.robotic@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'traffic_light_detector = traffic_light_detection.traffic_light_detection:main',
            'traffic_color_detector = traffic_light_detection.traffic_color_detection:main',
            'hsv_helper = traffic_light_detection.hsv_helper:main',
        ],
    },
)
