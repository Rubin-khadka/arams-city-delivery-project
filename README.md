# ARAMS_project_city_delivery

this is the project work for the arams class<br>
done by<br>
**Rubin Khadka Chhetri**<br>
**Marco Ruchay**<br>

Here are some information about installation and running everything.
The documentation is placed in the **/documation folder**.

## Repository structure

The **root** of this repository **is** the **src** folder.<br>
So you if you pull it the first time<br>
you have **rename** it to "src".<br>
<br>
The packeges like "arams_city" are included as git submodules.<br>
There are seperate commands for this. read section "clone" for this.<br>

The Packages:<br>
**launchbox** contains launchfiles for combined launching

The Package<br>
**my_prius_navigation** includes everthing which has to do with navigation<br>
like
- slam (for initial mapping)
- localization
- navigation
- goal management
    here are two options:
    - goal_manger_simple (for use without the project task launchfile, choosing goals randomly on its own)
    - goal_manager (asking for goals from GetGoals server)

folder structure:<br>
- /launch contains the launchfiles the these task<br>
- /conifg contains parameters<br>
- /maps the maps of corse<br>
- /my_prius_navigation contains<br>
    - cmd_vel_bypass node for topic forwarding from /cmd_vel_nav to /prius/cmd_vel
    - (goal_manager asks for delivery points and hands them over to naviagtion)

**traffic_light_detection** package contains everything that has to do with traffic light detection<br>
including yolo weights and cfg files
- traffic_light_detection node detects the traffic light using yolo and darknet 
- traffic_color_detection node then detects the color of the detected traffic light.<br>
Based on this detection the behaviour of the vehicle at traffic light is detemined.

folder structure:<br>
- /launch contains launch file that launch both nodes
- /weights contains all necessary yolo files like weights, cfg and class
- /traffic_light_detection
    - traffic_light_detection.py detect traffic light ROI and forwards to topic /traffic_light_roi
    - traffic_color_detection.py detect traffic color and puclishes detected color to /traffic_light_color




## Getting started (read before action)

### apt install stuff

- have a look into https://git.fh-aachen.de/mascor-public/arams-city for their "apt install" stuff<br>
(and **only that for now**, because the packeges of that repo are included in this repository as submodules)

- additionaly you need:
```
sudo apt install ros-humble-libg2o
```

### clone

Crate a new workspace
```
mkdir ~/my_new_ws
cd ~/my_new_ws
```

clone https://git.fh-aachen.de/mr7143s/arams_project_city_delivery.git<br>
and initialize submodules
```
git clone https://git.fh-aachen.de/mr7143s/arams_project_city_delivery.git
git submodule init
git submodule update
```

change the name of this cloned repository to "src" since it only contains the packages
```
mv arams_project_city_delivery src
```

**important Note:** in<br>
my_prius_navigation/conig/nav2_params.yaml<br>
you have to change the 
default_nav_to_pose_bt_xml
to your own direktory.
It is currently a absolut path and i dont know how to make it relative. <br>

traffic_light_detection.py <br>
Please change directory of darknet to your own darknet(line 10)<br>
Ignore the import darknet error<br>
And replace "/home/rubin/ros2_ws" to your own workspace directory (line 26,27,28).<br>
Yolo model is loaded here, relative path is not working here as well.<br>

build it
```
colcon build
#colcon build --packages-ignore teb_local_planner teb_msgs

source ~/arams_project_city_delivery/install/locale_setup.bash
```

## Run

launch city and spawn prius
```
ros2 launch launchbox city.launch.yaml
```
wait a bit, than<br>
launch localization, navigation, and cmd_vel bypass, 
```
ros2 launch launchbox navigation.launch.yaml
```
launch traffic light detection node and traffic light color node<br>
**important Note:**  Do not forget to change the directories as instructed<br>
```
ros2 launch traffic_light_detection traffic_light.launch.yaml
```
and finaly to get the goals and make the care move<br>
start the goalserver from arams task without the firetrucks (if it is not already running)
and the goal managager
```
#ros2 ros2 launch arams_city project_task.launch.py
ros2 launch goal_server goal_server.launch.yaml
ros2 run my_prius_navigation goal_manager_node --ros-args -r use_sim_time:=True
```


## Update

```
git pull
git submoldule update --remote
```

# Issues

## costmap 
firetrucks are note recognized by the costmap

## navigation
only the most basic planners are useable for us.<br>
more advanced which make nice truns so the car can drive forwad again, hat serious problems.<br>
see videos.

## localisation
localization is not sufficient.<br>
AMCL loses orientation after max two curves.<br>
Which makes the hole navigation not work.<br>
It jumps to a different places of the map.<br>
Have a look in the videos/issues/localization/

## mapping
launch city and spawn prius
```
ros2 launch launchbox city.launch.yaml
```
launch mapping
```
ros2 launch my_prius_navigation slam_toolbox.launch.yaml
```
drive one cirle and see that the loop is not closed.<br>
at the first intersection the roads dont line up

## car model
when i was mapping and driving the prius withe the joystickwindow<br>
i notesd the the prius sometimes does not drive the kurve.<br>
the wheels were turnd but i slided just forward,<br>
which was not only seeable on the map but also in the camera<br>
so the car was realy sliging straigt.

### Note: ###
Please take a look at the documentation folder<br>
Documetation in there contains all the issues we came across during this project <br>
and the methods we have taken to overcome those issues<br>
also all the issues that are still there are listed there<br>

