

# ARAMS_project_city_delivery DOCU

this is the project work for the arams class<br>
done by<br>
**Rubin Khadka Chhetri**<br>
**Marco Ruchay**<br>

There is a working video in this folder<br>
I'm sorry about the quality, but my PC was not able to handle<br>
a screencast and the ros stuff at the same time.<br>
I started the recording after the car reached the first goal.<br>
The car was able to reach three more goals until the navigation failed due to a timeout.<br>
There are also more videos and images in the /videos folder.<br>

# Navigation

## Slam (generating a map)
We were not able to get the "scan matching" functioning<br>
so that, after driving one circle around the the Block,<br>
the crossing is recognized and the map is squeezed,<br>
so that the streets line up again.<br>
Increasing the search radius (and adjusting other parameters) didn't help.<br>
When you have a look into my_prius_navigation/maps<br>
you will see that most of the maps have doubled streets.<br>

The solution was a little counter intuitive:<br>
Turning scan matching off.<br>
This is working, because this is a simulation and the odometry is perfect<br>
The result is a good map with streets which are even pixle aligned.<br>
my_prius_navigation/maps/<br>
- my-map_2023-07-28.pgm is a partial good map<br>
- full_map.pgm is the complete map which is used in the final result<br>

The default resolution is 0.05 m/px<br>
Because processing power was a big issue with our PCs<br>
and the the computing effort scales with the square of the resolution<br>
we reduced the resolution of the map.<br>
Anyway a such a resolution is not necessary to get the navigaton right.<br>
We chose a resolution of 0.4 m/px.<br>

## Delivery positions
For the delivery positons we were given a picture with 15 rough positions.<br>
To figure out the coordinates relative to "map" we made a lauchfile<br>
my_prius_navigation/launch/delivery_points_tf.launch.yaml<br>
with 15 static transforms which represent the delivery places.<br>
This way they can be be displayed in rviz.

## Localization
The used map (full_map) is started with this launch.<br>
This is the currently used is:<br>
my_prius_navigation/localization.launch.yaml<br>
There are some issues with the localization,<br>
that it twists alot when corners where driven<br>
and somtimes just jumps away when got too bad<br>
can be seen in /videos/issues/localization/<br>

## Navigation

### cmd_vel Topic forwarding

To get the car running at all, the topic "/cmd_vel_nav" form the navigation<br>
needs to be remaped to cars /prius/cmd_vel.<br>
For that case we wrote a node "cmd_vel_bypass_node" which republishes this topic<br>
and can be found in my_prius_navigation/my_prius_navigation/cmd_vel_bypass.py.<br>
This node also playes a roll in stoping at traffic lights by listening on the "stop_signal" topic.<br>

### Planner and Controller
(This was the most time consuming part and i wrote many emails which didn't help in the end.)<br>
The current solution uses the navigation form the third labsession<br>
which is meant to work with the turtlebot.<br>
The driving model there is a differcial drive, but the prius is not.<br>
But if we consider the middel of the rear axel of a ackerman drive,<br>
it behaves similar, exept it can not rotate in place.<br>
So we made a static transform rear_axel_center and use this as the base_link for naviagtion<br>
![rear_axel_center](driving_a_corner.png "rear axle center as base_link")

We alterd the standart behavior tree an throw out the "spin" recovery<br>
**important note:** in the nav2_params.yaml there is an absolute path to the bahaviortree.<br>
you have to adjust that with your working directory.<br>
I know it's ugly, but a relative path was not accepted.<br>

The "navfn" planner only plans direct connections between the car and the goal and does not considre orientation.<br>
If the path would start out perpendicular to the car we would have a problem,<br>
but with the structure we face here<br>
(delivery points only in streets not on crossings) this does not happen.<br>
Also the car does drive the hole path either forward or backward.<br>
The current navigation can be found in:<br>
my_prius_navigation/launch/navigation.launch.py<br>
my_prius_navigation/config/nav2_params.yaml<br>

We also tried other planners which would have made a beautiful path and a nice<br>
behavior. For example the smac planner.<br>
With the option "DUBIN" it makes loops to turn which don't fit in a street,<br>
so the option "REEDS_SHEPP" is the way to go which allows forward and backward movment<br>
to turn beautifully on a crossing like everone would do intuitivly in the real world.<br>
Like here:<br>
![smac planner with REEDS_SHEPP](smac_plan.png "smac planner with REEDS_SHEPP")
The problem here was, that the car got stuck on these direktion changes,<br>
because the local planner (controller) was jumping between the forward and backward path,<br>
because both paths were in reach for the controller.<br>
An other problem was, that in the corners the path was adjusted to the current position insted of followed,<br>
so the car pulled the path with it instead of turning.<br>
Can be seen in:<br>
videos/issues/2023-07-25 path gets adjusted insted of steering.webm<br>

We also tried to change the controller to "teb-local-planner",<br>
which most of the time had the issue that it uses minimal speeds less than 0.1<br>
and the path gets messed up completely.<br>
can be seen in:
videos/issues/navigation/2023-07-27_smac_and_teb.webm<br>

### Costmaps and Footprint
Because the car is not driving very accurat we increased the inflation radius drasticly.<br>
We also tried using the Footprints, to discribe the dimensions of the car more accurate,<br>
but this caused the inflation layer to decrease somehow, so we got back to the circle.<br>
The circle is located at the back of the car because this is the center of navigation.<br>
This is a little ugly bcause the car does not know about its front, but with all the other parameters tweaked, the car turns early enough in the corners and it functions.<br>

### Goal Manager
The goal manager is the node which gets a list of goals form the GetGoals server<br>
stores them and hands them over to the navigaion piece by piece.<br>
This can be found in my_prius_navigation/my_prius_navigation/goal_manager.py.<br>
There are two nested loops in the main logic (at the bottom)<br>
The outer itterates thrue each Goal which was given from the Server.<br>
The inner just sends a Goal again in case it is aborted.<br>
This helps a little with the issue (i wrote some of the many emails about)<br>
that on the the navigation gets aborted some times for no reason.<br>
We started with the autoexplorer template form labsession and rewrote the node as a class<br>
Then we added the requst to GetGoals and the controlllogic.<br>
The coordinates are hardcoded at the top<br>

# Image Processing

### Traffic light detection
The darknet_ros package suggested in the tutorial do not work with ros2. <br>
The package was giving error while building the workspace. <br>
We spent many hours trying to fix this package to work.<br>

The solution was to create our own node for the traffic light detection.<br>
The traffic light detection node uses the darknet package directly to do the traffic light detection<br>

The split.py and arams.data files provided in the tutorial is also not consistent with the google colab.<br>
After fixing those two files the google colab worked and we were able to train the dataset.<br>

The other thing is the amount of data and time it takes to train these model is massive. <br>
First we trained the model with about 200 images, 2nd was with 500 and 3rd was with 800.<br>
And it is still not properly trained. <br>
It does detects all the traffic light but you have be close to the traffic lights.<br>
like here:<br>
![traffic yolo detection](traffic_yolo_detection.png "traffic light detection by model")
its already crossed the intersection and now its detecting the traffic light. <br>
Trained the model 3 times still did not work properly.<br>

After the traffic light detection it does publish the traffic light ROI properly.<br>
like here:<br>
![traffic light roi](traffic_light_roi.png "traffic light roi")

**Important note:** <br>
In traffic_light_detection.py file, please change directory of darknet to your own (line 10)<br>
Ignore the import darknet error<br>
Also replace "/home/rubin/ros2_ws" to your own workspace directory where yolo model is loaded (line 26,27,28).<br>
Relative path was not working, it only worked with absolute path<br>
We tried to fix it but it just does not work.<br>

### Traffic light color detection
This node is working properly as it is supposed to be. <br>
It subscribes to traffic light roi and does necessary image processing to extract traffic light color.<br>
like here: the image above is processed to get following image,<br>
![processed roi image](processed_img_opencv.png "processed roi image")<br>
After the image processing, this node publishes the detected color as string.<br>
cmd_vel_bypass node subscribes to the published detected color.<br>
This node then stops the car according to received string message. <br>

The challenge while writing this node was getting the proper HSV and other parameter values.<br>

The main problem here is that it takes a long time to process an image.<br>
Sometimes the car already crosses the traffic light before it receives red color indication.<br>
It then tries to stop the car according to the message.<br>
It slows down the car , then it receives another message and start to drive normally.<br>

### Approach
One of the main challenge was to select a traffic light from bunch of traffic lights in an intersection.<br>
We considered or tried to use three methods to solve this: <br>

- First was to use the furthest traffic light. Since the city uses american style traffic light, <br>
  the furthest one is the appropriate traffic light. This one is not working properly as the Yolo model is not<br>
  trained properly. <br>
- Another is to just use the closest traffic light. Since the model detect traffic light when it is close, <br>
  this method is much more easy to approach. The idea is to calculate the area of bounding boxes and use the <br>
  biggest area. Biggest area means closest traffic light. <br>
- The last one was hardest to implement but we were able to implement it.<br>
  This is also much more logical solution. We got orientation of the car using odom "/prius/odom. <br>
  Then the angle between all the detected traffic light and car's orientation is calculated. <br>
  The one with smallest angle is selected and then a single traffic light ROI is published. <br>
  The idea here is to select traffic light which is straight to the car's orientation.<br>

Instead of making one more node to do this, its done directly in traffic detection node before publishing the ROI.<br>

### Issues
  -As mentioned, the yolo model isnt properly trained so the traffic detection system is not that pretty.<br>
  -The car do not turn around, so if the delivery destination is opposite to current position, the car just <br>
   drives backward till the new destination is reached.<br> 
  -Also the image processing take lot of time to process, so the car receives the traffic light color message<br>
   a bit late.