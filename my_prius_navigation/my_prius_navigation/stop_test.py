#!/usr/bin/env python3


import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from std_msgs.msg import Bool as Bool_msg


class MyPublisherClass(Node):


    def __init__(self):
        super().__init__('stop_test')
        self.mypub = self.create_publisher(Bool_msg, 'stop_signal', 1)
        self.create_timer(8, self.mytimercallback)

        self.state = False


    def mytimercallback(self):
        mymsg = Bool_msg()

        mymsg.data = self.state
        print(self.state)

        self.state = not self.state

        self.mypub.publish(mymsg)


def main():
    rclpy.init()
    publishernode = MyPublisherClass()

    try:
        rclpy.spin(publishernode)
    except KeyboardInterrupt:
        pass

    publishernode.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()