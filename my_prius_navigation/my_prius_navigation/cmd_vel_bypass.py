#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from std_msgs.msg import String

class twist_sub_c(Node):

	def __init__(self):
	
		super().__init__('cmd_vel_bypass')
		self.subscription = self.create_subscription(Twist, '/cmd_vel_nav', self.sub_callback, 10)
		self.publisher = self.create_publisher(Twist, '/prius/cmd_vel', 1)
		self.traffic_light_color = None
		self.create_subscription(String, 'traffic_light_color', self.traffic_light_callback, 10)

	def sub_callback(self, msg):
		if self.traffic_light_color == 'red':
			stop_msg = Twist()
			stop_msg.linear.x = 0.1
			stop_msg.angular.z = 0.0
			self.publisher.publish(stop_msg)
		else:
			self.publisher.publish(msg)

	def traffic_light_callback(self, msg):
		self.traffic_light_color = msg.data


def main():

	rclpy.init()
	twist_sub = twist_sub_c()
	try:
		rclpy.spin(twist_sub)
	except KeyboardInterrupt:
		pass
	twist_sub.destroy_node()
	rclpy.shutdown()


if __name__ == '__main__':
	main()