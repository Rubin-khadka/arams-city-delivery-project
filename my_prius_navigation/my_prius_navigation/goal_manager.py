#!/usr/bin/env python3

from math import sqrt
import sys
import rclpy
from rclpy.node import Node
from rclpy.action import ActionClient
from action_msgs.msg import GoalStatus

import random

from nav2_msgs.action import NavigateToPose
from geometry_msgs.msg import Point, Quaternion

from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener


from arams_city_interfaces.srv._get_goals import GetGoals
import arams_city_interfaces.msg



number_of_goals = 10


#position generated out of the delivery_points_tf.launch.yaml
delivery_points = [(-14.0, 68.0),
(-14.0, 27.0),
(-29.0, -1.0),
(-14.0, -28.0),
(-37.0, -45.0),
(-14.0, -68.0),
(0.0, -1.0),
(18.0, -44.0),
(29.0, -1.0),
(44.0, 9.0),
(46.0, -25.0),
(66.0, 0.0),
(87.0, 46.0),
(89.0, -1.0),
(90.0, -47.0)]




class goal_manager_node(Node):

	def __init__(self):
		super().__init__('goal_manager')


		#self.tf_buffer = Buffer()
		#self.tf_listener = TransformListener(self.tf_buffer, self)

		self.city_goal_client = self.create_client(GetGoals, "goal_server/get")

		while not self.city_goal_client.wait_for_service(timeout_sec=2.0):
			print("City GetGoal not running ..")

		self.city_request = GetGoals.Request()

		self.delivery_list = list()


  		# create Action Client object with desired message type and action name
		self.nav_to_pose_client = ActionClient(self, NavigateToPose, 'navigate_to_pose')

  		# wait for action server to come up
		while not self.nav_to_pose_client.wait_for_server(timeout_sec=2.0):
			print("Navigation Server not running; waiting...")

	def city_send_request(self):

			self.city_request.amount = number_of_goals
			self.future = self.city_goal_client.call_async(self.city_request)
			rclpy.spin_until_future_complete(self, self.future)
			return self.future.result()


	def sendGoal(self, position, orientation):

		goal = NavigateToPose.Goal()
		goal.pose.header.frame_id = "map"

		goal.pose.header.stamp = self.get_clock().now().to_msg()

		#transform = self.tf_buffer.lookup_transform("delivery_03", "map", rclpy.time.Time(), rclpy.duration.Duration(seconds=5.0))
		#print(transform)
  
		goal.pose.pose.position = position
		goal.pose.pose.orientation = orientation

		print("at position X: " + str(goal.pose.pose.position.x) + " Y: " + str(goal.pose.pose.position.y))

		self.send_goal_future = self.nav_to_pose_client.send_goal_async(goal)
		rclpy.spin_until_future_complete(self, self.send_goal_future)

		goal_handle = self.send_goal_future.result()

		if not goal_handle.accepted:
			print("Goal was rejected")
			self.nav_to_pose_client.destroy()
			self.destroy_node()
			rclpy.shutdown()
			sys.exit(0)

		print("auf geht's!")

		return goal_handle

	def checkResult(self, goal_handle):
		"""Check for task completion while blocking further execution"""
		get_result_future = goal_handle.get_result_async()

		rclpy.spin_until_future_complete(self, get_result_future)

		status = get_result_future.result().status

		return status

	def generatePosition(self, delivery):
		
		delivery -= 1
		delivery %= 15

		position = Point()

		position.x = delivery_points[delivery][0]
		position.y = delivery_points[delivery][1]

		position.z = 0.0
		return position

	def generateOrientation(self):
		
		quat = Quaternion()
		quat.w = 0.0
		quat.x = 0.0
		quat.y = 0.0
		quat.z = sqrt(1 - quat.w*quat.w)
		return quat
	
	def extract_number(self, goal_str):

		extracted = goal_str[0:goal_str.index(":")]
		#print(extracted)

		return int(extracted)

def main():
	rclpy.init()
	node = goal_manager_node()

	result = node.city_send_request()
	node.delivery_list = result.goals

	print("the complete delivery list:")
	print(node.delivery_list)


	# itterate thru ever goal from GetGoals server
	for each_goal in node.delivery_list:

		print("next delivery:")
		print(each_goal)
		goal_number = node.extract_number(each_goal)
		
		position = node.generatePosition(goal_number)
		orientation = node.generateOrientation()

		# send a goal again and again as long as its not reached
		while True:

			goal_handle = node.sendGoal(position, orientation)
			status = node.checkResult(goal_handle)

			if status == GoalStatus.STATUS_ABORTED:
				print("hoppla, nochmal")
				continue

			elif status == GoalStatus.STATUS_SUCCEEDED:
				print("delivery done !")
				break

			else:
				print("upps")

	print("complete delivery done")



	try:
		rclpy.spin(node)
	except KeyboardInterrupt:
		pass

	node.nav_to_pose_client.destroy()
	rclpy.shutdown()