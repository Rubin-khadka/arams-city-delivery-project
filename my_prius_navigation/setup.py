
import os
from glob import glob
from setuptools import setup

package_name = 'my_prius_navigation'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.*')),
        (os.path.join('share', package_name, 'config'), glob('config/*')),
        (os.path.join('share', package_name, 'maps'), glob('maps/*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='marco',
    maintainer_email='marco.ruchay@alumni.fh-aachen.de',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'cmd_vel_bypass_node = my_prius_navigation.cmd_vel_bypass:main',
            'goal_manager_simple_node = my_prius_navigation.goal_manager_simple:main',
            'goal_manager_node = my_prius_navigation.goal_manager:main',
            'goal_manager_with_lights_node = my_prius_navigation.goal_manager_with_lights:main',
            'stop_test_node = my_prius_navigation.stop_test:main',
        ],
    },
)
